import {Component, OnInit} from '@angular/core';
import {HttpClientService} from 'src/app/core/services/http-client.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
  noteList: any;

  constructor(
      private http: HttpClientService,
  ) {}

  ngOnInit(): void {
    this.getNotes();
  }

  getNotes() {
    this.http.httpGet('note/find-all').subscribe(d => {
        this.noteList = d;
    });
  }
}
