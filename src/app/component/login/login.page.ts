import {Component, OnInit} from '@angular/core';
import {ToastController} from '@ionic/angular';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';

declare var $;
@Component({
  selector: 'app-login',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;

  constructor(
      private toastController: ToastController,
      private formBuilder: FormBuilder,
      private router: Router,
  ) {}

  ngOnInit(): void {
    $('.hi').fadeIn(1000, () => {
      $('.field').fadeIn('fast');
      $('.btn').removeClass('hidden');
    });

    this.build();
  }

  build() {
    this.loginForm = this.formBuilder.group({
      username: '',
      password: '',
    });
  }

  async toast() {
    const toast = await this.toastController.create({
      message: 'Invalid credentials',
      duration: 2000,
      color: 'danger',
      cssClass: 'toast'
    });

    await toast.present();
  }

  join() {
    if (this.loginForm.get('username').value != 'admin' || this.loginForm.get('password').value != '1234') {
      this.toast();
    } else {
      this.router.navigate(['home']);
    }
  }
}
