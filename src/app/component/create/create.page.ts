import {Component, OnInit} from '@angular/core';
import {HttpClientService} from 'src/app/core/services/http-client.service';
import {ToastController} from '@ionic/angular';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
    selector: 'app-create',
    templateUrl: 'create.page.html',
    styleUrls: ['create.page.scss'],
})
export class CreatePage implements OnInit{
    noteForm: FormGroup;

    constructor(
        private http: HttpClientService,
        private toastController: ToastController,
        private router: Router,
        private formBuilder: FormBuilder,
    ) {}

    ngOnInit(): void {
        this.build();
    }

    build() {
        this.noteForm = this.formBuilder.group({
            note: '',
        });
    }

    async toast(success?) {
        const toast = await this.toastController.create({
            message: success ? 'Note was created' : 'Note field is required',
            duration: 2000,
            cssClass: success ? 'toast' : 'toast danger'
        });

        await toast.present();
    }

    save() {
        if (!this.noteForm.get('note').value) {
            this.toast();
        } else {
            this.http.httpPost('note/create', this.noteForm.controls).subscribe(() => {
                this.toast(true);
                this.noteForm.reset();
                this.router.navigate(['home']);
            });
        }
    }
}
