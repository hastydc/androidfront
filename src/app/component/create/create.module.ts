import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CreatePage } from './create.page';

import { CreatePageRoutingModule } from './create-routing.module';
import {MenuModule} from 'src/app/shared/menu/menu.module';
import {FooterModule} from 'src/app/shared/footer/footer.module';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CreatePageRoutingModule,
        MenuModule,
        FooterModule,
        ReactiveFormsModule
    ],
    declarations: [CreatePage]
})
export class CreatePageModule {}
