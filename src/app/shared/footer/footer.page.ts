import {Component, Input} from '@angular/core';

@Component({
    selector: 'app-footer',
    templateUrl: 'footer.page.html',
    styleUrls: ['footer.page.scss'],
})
export class FooterPage {
    @Input('back') back: any;

    constructor() {}
}
