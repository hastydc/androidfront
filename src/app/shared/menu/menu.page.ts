import {Component, Input} from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector: 'app-menu',
    templateUrl: 'menu.page.html',
    styleUrls: ['menu.page.scss'],
})
export class MenuPage {
    @Input('page') page: string;

    constructor(
        private router: Router,
    ) {}

    signOut() {
        this.router.navigate(['']);
    }
}
