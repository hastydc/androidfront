import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import {FooterPage} from './shared/footer/footer.page';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./component/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./component/login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'create',
    loadChildren: () => import('./component/create/create.module').then(m => m.CreatePageModule)
  },
  {
    path: 'home/create',
    redirectTo: 'create',
    pathMatch: 'full'
  },
  {
    path: 'create/home',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
